﻿namespace _kalkulator
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bPlus = new System.Windows.Forms.Button();
            this.tbInputA = new System.Windows.Forms.TextBox();
            this.tbInputB = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.bMultiply = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bPlus
            // 
            this.bPlus.Location = new System.Drawing.Point(12, 64);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(50, 23);
            this.bPlus.TabIndex = 0;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = true;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // tbInputA
            // 
            this.tbInputA.Location = new System.Drawing.Point(12, 12);
            this.tbInputA.Name = "tbInputA";
            this.tbInputA.Size = new System.Drawing.Size(100, 20);
            this.tbInputA.TabIndex = 1;
            // 
            // tbInputB
            // 
            this.tbInputB.Location = new System.Drawing.Point(12, 38);
            this.tbInputB.Name = "tbInputB";
            this.tbInputB.Size = new System.Drawing.Size(100, 20);
            this.tbInputB.TabIndex = 2;
            // 
            // tbOutput
            // 
            this.tbOutput.Location = new System.Drawing.Point(12, 139);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(100, 20);
            this.tbOutput.TabIndex = 3;
            // 
            // bMultiply
            // 
            this.bMultiply.Location = new System.Drawing.Point(12, 93);
            this.bMultiply.Name = "bMultiply";
            this.bMultiply.Size = new System.Drawing.Size(50, 23);
            this.bMultiply.TabIndex = 4;
            this.bMultiply.Text = "*";
            this.bMultiply.UseVisualStyleBackColor = true;
            this.bMultiply.Click += new System.EventHandler(this.bMultiply_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(133, 171);
            this.Controls.Add(this.bMultiply);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.tbInputB);
            this.Controls.Add(this.tbInputA);
            this.Controls.Add(this.bPlus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.TextBox tbInputA;
        private System.Windows.Forms.TextBox tbInputB;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.Button bMultiply;
    }
}

